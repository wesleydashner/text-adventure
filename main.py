from random import randint
from random import choice
from time import sleep

# Text Adventure 2.1.2

sleep_time = 2


class Weapon:

    def __init__(self, name, min_damage, max_damage):
        self.name = name
        self.min_damage = min_damage
        self.max_damage = max_damage


stick = Weapon('stick', 1, 5)
dagger = Weapon('dagger', 5, 7)
sword = Weapon('sword', 10, 15)


class Armor:

    def __init__(self, name, min_protect, max_protect):
        self.name = name
        self.min_protect = min_protect
        self.max_protect = max_protect


leather_armor = Armor('leather armor', 1, 3)
steel_armor = Armor('steel armor', 2, 5)


class Player:

    weapon = stick
    armor = None
    gold = 0
    hp = 50
    x_location = 3
    y_location = 3
    level = 0
    victory = False

    def is_alive(self):
        if self.hp > 0:
            return True
        else:
            return False

    def move_north(self):
        self.y_location -= 1

    def move_south(self):
        self.y_location += 1

    def move_east(self):
        self.x_location += 1

    def move_west(self):
        self.x_location -= 1


def get_available_moves():

    available_moves = []

    player.y_location -= 1
    for i in rooms:
        if player.x_location == i.x and player.y_location == i.y:
            available_moves.append('n')
    player.y_location += 1

    player.y_location += 1
    for i in rooms:
        if player.x_location == i.x and player.y_location == i.y:
            available_moves.append('s')
    player.y_location -= 1

    player.x_location += 1
    for i in rooms:
        if player.x_location == i.x and player.y_location == i.y:
            available_moves.append('e')
    player.x_location -= 1

    player.x_location -= 1
    for i in rooms:
        if player.x_location == i.x and player.y_location == i.y:
            available_moves.append('w')
    player.x_location += 1

    return available_moves


class Enemy:

    def __init__(self, hp, min_damage, max_damage, name):
        self.hp = hp
        self.min_damage = min_damage
        self.max_damage = max_damage
        self.name = name

    def is_alive(self):
        if self.hp > 0:
            return True
        else:
            return False


class EmptyRoom:

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.discovered = False

    def room_entered(self):
        print('Nothing to see in this room. You must forge onwards!')


class FindWeaponRoom:

    def __init__(self, x, y, item):
        self.x = x
        self.y = y
        self.item = item
        self.discovered = False

    def room_entered(self):

            print('You find a {} on the ground.'.format(self.item.name))
            current_weapon = player.weapon
            sleep(sleep_time)
            move = input('Would you like to trade your {} for the {} you\'ve found? [y/n] '.format(player.weapon.name, self.item.name))
            while move not in ["y", "n"]:
                print("'{}' is not a valid input.".format(move))
                move = input('Would you like to trade your {} for the {} you\'ve found? [y/n]? '.format(player.weapon.name,
                                                                                                 self.item.name))
            if move == 'y':
                player.weapon = self.item
                self.item = current_weapon


class FindArmorRoom:

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.item = armor1
        self.discovered = False

    def room_entered(self):

            if not self.item:
                print('Nothing to see in this room. You must forge onwards!')
                return
            print('You find {} on the ground.'.format(self.item.name))
            current_armor = player.armor
            sleep(sleep_time)
            if player.armor == None:
                move = input('Would you like to put the {} on? [y/n] '.format(self.item.name))
                while move not in ["y", "n"]:
                    print("'{}' is not a valid input.".format(move))
                    move = input('Would you like to put the {} on? [y/n] '.format(self.item.name))
                if move == 'y':
                    player.armor = self.item
                    self.item = current_armor
            elif player.armor:
                move = input('Would you like to trade your {} for the {} you\'ve found? [y/n] '.format(player.armor.name, self.item.name))
                while move not in ["y", "n"]:
                    print("'{}' is not a valid input.".format(move))
                    move = input('Would you like to trade your {} for the {} you\'ve found? [y/n] '.format(player.weapon.name,
                                                                                                     self.item.name))
                if move == 'y':
                    player.armor = self.item
                    self.item = current_armor


class EnemyRoom:

    def __init__(self, x, y, enemy, alive_text, dead_text):
        self.x = x
        self.y = y
        self.enemy = enemy
        self.alive_text = alive_text
        self.dead_text = dead_text
        self.discovered = False

    def room_entered(self):

        if self.enemy.is_alive():
            print(self.alive_text)
            sleep(sleep_time)

            if player.armor:
                damage = randint(self.enemy.min_damage, self.enemy.max_damage) - randint(player.armor.min_protect,
                                                                                         player.armor.max_protect)
            if not player.armor:
                damage = randint(self.enemy.min_damage, self.enemy.max_damage)
            if damage < 0:
                damage = 0
            player.hp -= damage
            if player.hp < 0:
                player.hp = 0

            print('The {} deals {} damage to you!'.format(self.enemy.name, damage))
            sleep(sleep_time)
            print('You have {} HP remaining.'.format(player.hp))

            if not player.is_alive():
                print('The {} kills you.'.format(self.enemy.name))
                return

            sleep(sleep_time)

            while player.hp > 0:

                move = input('Attack or Flee? [a/f] ')
                while move not in ["a", "f"]:
                    print("'{}' is not a valid input.".format(move))
                    move = input('Attack or Flee? [a/f] ')
                if move == 'a':
                    damage = randint(player.weapon.min_damage, player.weapon.max_damage) + (player.level * 2)
                    print('You use your {} against the {}, dealing {} damage!'.format(player.weapon.name, self.enemy.name, damage))
                    self.enemy.hp -= damage
                    sleep(sleep_time)

                    if self.enemy.is_alive():
                        print('The {} has {} HP remaining.'.format(self.enemy.name, self.enemy.hp))
                        sleep(sleep_time)
                        if player.armor:
                            damage = randint(self.enemy.min_damage, self.enemy.max_damage) - randint(player.armor.min_protect, player.armor.max_protect)
                        if not player.armor:
                            damage = randint(self.enemy.min_damage, self.enemy.max_damage)
                        damage - (player.level * 2)
                        if damage < 0:
                            damage = 0
                        player.hp -= damage
                        if player.hp < 0:
                            player.hp = 0

                        print('The {} deals {} damage to you!'.format(self.enemy.name, damage))
                        sleep(sleep_time)
                        print('You have {} HP remaining.'.format(player.hp))

                        if not player.is_alive():
                            print('The {} kills you.'.format(self.enemy.name))

                    else:
                        print('You killed the {}!'.format(self.enemy.name))
                        sleep(sleep_time)
                        player.level += 1
                        print('You leveled up! You are now level {}.'.format(player.level + 1))
                        break

                if move == 'f':

                    print('You flee from the battle.')
                    sleep(sleep_time)
                    print('Unfortunately, you lost all sense of direction during the fight and aren\'t sure which direction you have run away to.')
                    sleep(sleep_time)

                    move = choice(get_available_moves())

                    if move == 'n':
                        player.move_north()
                    if move == 's':
                        player.move_south()
                    if move == 'e':
                        player.move_east()
                    if move == 'w':
                        player.move_west()

                    for i in rooms:
                        if player.x_location == i.x and player.y_location == i.y:
                            i.room_entered()
                            break

                    break

        else:
            print(self.dead_text)


class FindGoldRoom:

    def __init__(self, x, y, gold):
        self.x = x
        self.y = y
        self.gold = gold
        self.acquired = False
        self.discovered = False

    def room_entered(self):
        if not self.acquired:
            move = input('You find {} gold on the ground! Would you like to pick it up? [y/n] '.format(self.gold))
            while move not in ["y", "n"]:
                print("'{}' is not a valid input.".format(move))
                move = input('You find {} gold on the ground! Would you like to pick it up? [y/n] '.format(self.gold))

            if move == 'y':
                player.gold += self.gold
                print('You now have {} gold.'.format(player.gold))
                self.acquired = True
        else:
            print('Nothing to see in this room. You must forge onwards!')


class FindPotionRoom:

    def __init__(self, x, y, life_gain_min, life_gain_max, life_loss_min, life_loss_max):
        self.x = x
        self.y = y
        self.life_gain_min = life_gain_min
        self.life_gain_max = life_gain_max
        self.life_loss_min = life_loss_min
        self.life_loss_max = life_loss_max
        self.acquired = False
        self.discovered = False

    def room_entered(self):
        if not self.acquired:
            if self.life_loss_min == 0 and self.life_loss_max == 0:
                move = input('You find a blue potion on the ground. Would you like to drink it? [y/n] ')
                while move not in ["y", "n"]:
                    print("'{}' is not a valid input.".format(move))
                    move = input('You find a blue potion on the ground. Would you like to drink it? [y/n] ')
                if move == 'y':
                    print('You drink the potion and begin to feel your wounds heal.')
                    heal = randint(self.life_gain_min, self.life_gain_max)
                    player.hp += heal
                    sleep(sleep_time)
                    print('The healing potion restores {} HP. You have {} HP remaining.'.format(heal, player.hp))
                    self.acquired = True
            elif self.life_gain_min == 0 and self.life_gain_max == 0:
                move = input('You find a green potion on the ground. Would you like to drink it? ')
                if move == 'y':
                    print('You drink the potion and start to feel sick. You\'ve been poisoned!')
                    damage = randint(self.life_loss_min, self.life_loss_max)
                    player.hp -= damage
                    if player.hp < 0:
                        player.hp = 0
                    sleep(sleep_time)
                    print('Posion dealt {} damage. You have {} HP remaining.'.format(damage, player.hp))
                    self.acquired = True
        else:
            print('Nothing to see in this room. You must forge onwards!')


class LeaveRoom:

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.discovered = False

    def room_entered(self):
        print('You see light at the end of the tunnel you\'ve just entered. As you draw closer, you recognize it as sunlight!')
        sleep(sleep_time)
        print('Just before you reach the exit of the dungeon, a wizard appears directly in front of you.')
        sleep(sleep_time)
        print('You must pay him 10 gold in order to pass. You have {} gold.'.format(player.gold))
        sleep(sleep_time)
        if player.gold >= 10:
            move = input('Would you like to pay the wizard? ')
            if move == 'y':
                sleep(sleep_time)
                print('You pay the wizard his 10 gold and continue towards the light in the distance. You have escaped the dungeon alive!')
                player.victory = True
        else:
            print('You must find more gold in the dungeon before you can escape.')


def print_map():

    class Space():

        def __init__(self, x, y):
            self.x = x
            self.y = y
            self.print_value = ' '

    space1 = Space(1, 1)
    space2 = Space(2, 1)
    space3 = Space(3, 1)
    space4 = Space(4, 1)
    space5 = Space(5, 1)
    space6 = Space(1, 2)
    space7 = Space(2, 2)
    space8 = Space(3, 2)
    space9 = Space(4, 2)
    space10 = Space(5, 2)
    space11 = Space(1, 3)
    space12 = Space(2, 3)
    space13 = Space(3, 3)
    space14 = Space(4, 3)
    space15 = Space(5, 3)
    space16 = Space(1, 4)
    space17 = Space(2, 4)
    space18 = Space(3, 4)
    space19 = Space(4, 4)
    space20 = Space(5, 4)
    space21 = Space(1, 5)
    space22 = Space(2, 5)
    space23 = Space(3, 5)
    space24 = Space(4, 5)
    space25 = Space(5, 5)

    spaces = [space1, space2, space3, space4, space5, space6, space7, space8, space9, space10, space11, space12, space13, space14, space15, space16, space17, space18, space19, space20, space21, space22, space23, space24, space25]

    for room in rooms:
        for space in spaces:
            if room.x == space.x and room.y == space.y:
                if player.x_location == room.x and player.y_location == room.y:
                    space.print_value = 'X'
                elif room.discovered:
                    space.print_value = 'O'

    print(' ---' * 5)
    print('| ' + space1.print_value + ' |' + ' ' + space2.print_value + ' |' + ' ' + space3.print_value + ' |' + ' ' + space4.print_value + ' |' + ' ' + space5.print_value + ' |')
    print(' ---' * 5)
    print('| ' + space6.print_value + ' |' + ' ' + space7.print_value + ' |' + ' ' + space8.print_value + ' |' + ' ' + space9.print_value + ' |' + ' ' + space10.print_value + ' |')
    print(' ---' * 5)
    print('| ' + space11.print_value + ' |' + ' ' + space12.print_value + ' |' + ' ' + space13.print_value + ' |' + ' ' + space14.print_value + ' |' + ' ' + space15.print_value + ' |')
    print(' ---' * 5)
    print('| ' + space16.print_value + ' |' + ' ' + space17.print_value + ' |' + ' ' + space18.print_value + ' |' + ' ' + space19.print_value + ' |' + ' ' + space20.print_value + ' |')
    print(' ---' * 5)
    print('| ' + space21.print_value + ' |' + ' ' + space22.print_value + ' |' + ' ' + space23.print_value + ' |' + ' ' + space24.print_value + ' |' + ' ' + space25.print_value + ' |')
    print(' ---' * 5)


ogre1 = Enemy(30, 5, 10, 'ogre')
ogre2 = Enemy(30, 5, 10, 'ogre')
smallspider1 = Enemy(10, 1, 5, 'spider')
giantspider1 = Enemy(30, 1, 15, 'spider')
dragon1 = Enemy(50, 10, 20, 'dragon')

gold1 = randint(5, 15)
gold2 = randint(5, 15)

armor1 = choice([leather_armor, steel_armor])

room1 = EnemyRoom(3, 2, ogre1,
                        'An ogre steps out of a shadowy corner, holding a giant wooden club.',
                        'The corpse of an ogre rots on the ground, filling the room with a terrible stench.')
room2 = EnemyRoom(1, 5, ogre2,
                        'An ogre steps out of a shadowy corner, holding a giant wooden club.',
                        'The corpse of an ogre rots on the ground, filling the room with a terrible stench.')
room3 = EnemyRoom(5, 4, smallspider1,
                  'A large spider drops down from its web, ready to attack! It stands about as tall as your knees.',
                  'A dead spider lies on its back in the corner of the room.')
room4 = FindArmorRoom(4, 5)
room5 = EnemyRoom(5, 5, giantspider1,
                  'A huge spider jumps off the wall. It\'s large fangs meet your eyes just below eye level.',
                  'A huge spider lies dead in the center of the room.')
room6 = EnemyRoom(4, 1, dragon1,
                  'As soon as you walk through the doorway, a gigantic dragon flies down from its perch and meets you.',
                  'A slain dragon lies on the ground, defeated.')
room7 = FindWeaponRoom(3, 1, sword)
room8 = FindWeaponRoom(1, 2, dagger)
room9 = FindWeaponRoom(3, 5, dagger)
room10 = EmptyRoom(3, 3)
room11 = EmptyRoom(2, 3)
room12 = FindGoldRoom(1, 3, gold1)
room13 = FindGoldRoom(4, 3, gold2)
room14 = FindPotionRoom(2, 1, 5, 10, 0, 0)
room15 = FindPotionRoom(5, 3, 5, 10, 0, 0)
room16 = FindPotionRoom(2, 4, 5, 10, 0, 0)
room17 = FindPotionRoom(2, 5, 0, 0, 4, 7)
room18 = LeaveRoom(5, 1)

rooms = [room1, room2, room3, room4, room5, room6, room7, room8, room9, room10, room11, room12, room13, room14, room15, room16, room17, room18]

player = Player()

map_mode = None

while player.hp > 0 and not player.victory:

    if not map_mode:
        while map_mode not in ["y", "n"]:
            map_mode = input('Map mode [y/n]: ')
            if map_mode not in ["y", "n"]:
                print("'{}' is not a valid input.".format(map_mode))
        sleep(1)
        print('You wake up in the middle of a cold, dark room, with three doorways leading in different directions. ')
        sleep(sleep_time)
        print('A torch on the south wall fills the room with dim light. Your body aches as you stand up.')
        sleep(sleep_time)
        print('You must escape this dungeon. Unfortunately, all you have is a stick you\'ve just found and the clothes on your back.')

    sleep(sleep_time)

    base_x = player.x_location
    base_y = player.y_location

    for room in rooms:
        if room.x - player.x_location < 2 and room.x - player.x_location > -2:
            if room.y == player.y_location and room.y == player.y_location:
                room.discovered = True
        if room.y - player.y_location < 2 and room.y - player.y_location > -2:
            if room.x == player.x_location and room.x == player.x_location:
                room.discovered = True

    if map_mode == 'y':
        print_map()

    else:
        print('Available moves: ', end="")
        for i in get_available_moves():
            print(i + " ", end="")

    move = None

    while move not in ["n", "s", "e", "w"]:
        move = input('\nMove [n/s/e/w]: ')
        if move not in ["n", "s", "e", "w"]:
            print("'{}' is not a valid input.".format(move), end="")
    if move == 'n':
        player.move_north()
    if move == 's':
        player.move_south()
    if move == 'e':
        player.move_east()
    if move == 'w':
        player.move_west()

    sleep(0.5)

    for i in rooms:
        room_available = False
        if player.x_location == i.x and player.y_location == i.y:
            i.room_entered()
            room_available = True
            break

    if not room_available:
        print('There is no doorway leading in this direction.')
        player.x_location = base_x
        player.y_location = base_y

sleep(sleep_time)
if not player.victory:
    print('Game over.')

if player.victory:
    print('VICTORY!')

sleep(10)
